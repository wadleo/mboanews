<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require('api.php');

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::get('/article/{id}', 'WelcomeController@readArticle')->name('read_article');

Route::get('/category/{id}', 'WelcomeController@categoryArticles')->name('category_articles');

//auth routes
Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

//test scrapping
Route::get('/testscrapping', 'NewsController@scrapeArticles')->name('scrappe_article');
Route::get('/test', 'NewsController@scrapeArticles')->name('scrappe_article');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
//Admin index page
Route::get('/dashboard', ['as' => 'admin', 'uses' => 'AdminController@index']);

//sources CRUD
Route::get('/dashboard/source', ['as' => 'getSource', 'uses' => 'AdminController@getSources']);
Route::post('/dashboard/createSource', ['as' => 'createSource', 'uses' => 'AdminController@createSource']);
Route::get('/dashboard/getedit/source/{id}', ['as' => 'getUpdate', 'uses' => 'AdminController@getUpdate']);
Route::post('/dashboard/update/source/{id}', ['as' => 'updateSource', 'uses' => 'AdminController@updateSource']);
Route::get('/dashboard/delete/source/{id}', ['as' => 'deleteSource', 'uses' => 'AdminController@deleteSource']);