<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/*
|--------------------------------------------------------------------------
| Routes for the app (api routes)
|--------------------------------------------------------------------------
*/
Route::get('/api/get_categories', ['uses' => 'ApiController@getCategories'])->middleware('api');

Route::get('/api/get_sources', ['uses' => 'ApiController@getSources'])->middleware('api');

Route::get('/api/get_articles', ['uses' => 'ApiController@getArticles'])->middleware('api');

Route::get('/api/get_category_articles/{id} ', ['uses' => 'ApiController@getCategoryArticles'])->middleware('api');

Route::get('/api/get_article/{id}', ['uses' => 'ApiController@getArticle'])->middleware('api');
