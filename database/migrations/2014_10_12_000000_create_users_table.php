<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tel');
            $table->string('email', 100)->unique();
            $table->enum('type', ['user', 'publisher', 'admin']);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        $user = new User();
        $user->name = "Katchoua God's Will";
        $user->tel = "676782064";
        $user->email = "katchoua99@gmail.com";
        $user->password = bcrypt("katchoua");
        $user->save();

        $user1 = new User();
        $user1->name = "Jane Done";
        $user1->tel = "676782064";
        $user1->email = "jane@gmail.com";
        $user1->password = bcrypt("janejane");
        $user1->save();

        $admin = new User();
        $admin->name = "Administrator";
        $admin->tel = "676782064";
        $admin->email = "admin@admin.com";
        $admin->type = "admin";
        $admin->password = bcrypt("adminadmin");
        $admin->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
