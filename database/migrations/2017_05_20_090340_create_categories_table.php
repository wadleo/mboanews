<?php

use App\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('name_en');
            $table->string('name_fr');
            $table->string('description_en')->nullable();
            $table->string('description_fr')->nullable();
            $table->timestamps();
        });
        
        $cat = new Category();
        $cat->name_en = "Latest";
        $cat->name_fr = "Nouvelles";
        $cat->save();

        $cat1 = new Category();
        $cat1->name_en = "Politics";
        $cat1->name_fr = "Politiques";
        $cat1->save();

        $cat2 = new Category();
        $cat2->name_en = "Economy";
        $cat2->name_fr = "Economie";
        $cat2->save();

        $cat3 = new Category();
        $cat3->name_en = "Social";
        $cat3->name_fr = "Social";
        $cat3->save();

        $cat4 = new Category();
        $cat4->name_en = "Culture";
        $cat4->name_fr = "Culture";
        $cat4->save();

        $cat5 = new Category();
        $cat5->name_en = "Sports";
        $cat5->name_fr = "Sport";
        $cat5->save();

        $cat6 = new Category();
        $cat6->name_en = "Others";
        $cat6->name_fr = "Divers";
        $cat6->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
