<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'id', 'title', 'content', 'source_id', 'published_date', 'created_at', 'updated_at', 'category_id', 'sub_title', 'back_link'
    ];

    public function tags() {
    	return $this->hasMany('App\Tag');
    }

    public function image() {
        return $this->hasOne('App\Image');
    }

    public function hasImage() {
        if(!empty($this->image->url) && (strpos($this->image->url, '.jpg') > 0 || strpos($this->image->url, '.png') > 0 ||strpos($this->image->url, '.gif') > 0)) {
            return true;
        } else { 
            return false;
        }
    }

    public function validImage() {
        return $this->hasOne('App\Image')->where('url', 'like', '%.jpg%');
    }

    public function source() {
    	return $this->belongsTo('App\Source');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }
}
