<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //

    public function articles() {
    	$this->hasMany('App\Article');
    }
}
