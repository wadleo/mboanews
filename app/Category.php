<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name_en', 'name_fr', 'description_en', 'description_fr',
    ];

    //
    public function sources() {
    	return $this->hasMany('App\Source');
    }

    public function articles() {
    	return $this->hasMany('App\Article');
    }

    public function articlesCount() {
        return $this->articles()->selectRaw('category_id, count(*) as articles_count')->groupBy('category_id')
        ->first()['articles_count'];
    }

}
