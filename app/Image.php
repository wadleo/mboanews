<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'id', 'article_id', 'name', 'url', 'created_at', 'updated_at'
    ];

    public function article() {
    	return $this->belongsTo('App\Article');
    }
}
