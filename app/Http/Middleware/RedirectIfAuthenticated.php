<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->is('login') && $request->isMethod('post')) {
            if (\App\User::where([['email', $request->email], ['type', 'admin']])->exists()) {
                if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                    return redirect()->intended('/dashboard');
                }
            }
        }

        return $next($request);

    }
}
