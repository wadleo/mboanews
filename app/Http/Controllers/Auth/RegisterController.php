<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $categories = Category::all();
        \View::share('categories', $categories);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    public function register(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'name' => 'required|max:255|min:2|regex:/^[a-z-.(0-9)?*]+( [\w-.])+/i',
            'tel' => 'required|unique:users|min:9|numeric',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validate->fails()) {
            return redirect('/register')->withErrors($validate)->withInput();
        } else {

            //    Mail::send('emails.welcome', $data, function ($message) use($data){

            //     $message->from('hello@mboanews.com', 'testing this email');

            //     $message->to($data['email'])->subject("Welcome to Welcome to mboanews");

            // });

            $user = new User;
            $user->name = $request->name;
            $user->tel = $request->tel;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
            \Auth::login($user);
            return \Redirect()->to('/')->with('status', '<b>Hey there! Welcome to News_Local.</b> <br>Nice step taken!  Thanks for signing up. Good job! <br> <b><q>Faith is taking the first step even when you can\'t see the whole staircase.</q></b> <br> <i style="color: #52b6ec;">―Martin Luther King Jr.</i>');
        }
    }
//
//    protected function create(array $data)
//    {
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => bcrypt($data['password']),
//        ]);
//    }
}
