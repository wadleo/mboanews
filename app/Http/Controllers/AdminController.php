<?php

namespace App\Http\Controllers;

use App\Source;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
        $categories = Category::all();
        \View::share('categories', $categories);
    }

    /*
    |--------------------------------------------------------------------------
    | Index Function
    |--------------------------------------------------------------------------
    */
    //Dashboard(admin) index page
    public function index()
    {
        return view('admin.index');
    }

    /*
    |--------------------------------------------------------------------------
    | Sources CRUD Functions
    |--------------------------------------------------------------------------
    */
    public function getSources()
    {
        $sources = Source::all();

        return view('admin.pages.getSources', compact('sources'));
    }

    public function createSource(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required',
            'details' => 'required',
            'domain' => 'required',
            //'category_id' => 'required',
            ]);

        Source::create([
            'name' => $request->name,
            'url' => $request->url,
            'details' => $request->details,
            'domain' => $request->domain,
            'category_id' => $request->category_id,
            ]);

        return [
        'status' => 'success',
        'source' => 'Source created successfully!'
        ];
    }

    public function getUpdate($id)
    {
        $source = Source::find($id);
        
        return view('admin.pages.updateSource', compact('source'));
    }

    public function updateSource(Request $request ,$id)
    {

        $this->validate($request, [
            'name' => 'required',
            'url' => 'required',
            'details' => 'required',
            'domain' => 'required',
            'category_id' => 'required',
            ]);

        Source::where('id', $id)
        ->update([
            'name' => $request->name,
            'url' => $request->url,
            'details' => $request->details,
            'domain' => $request->domain,
            'category_id' => $request->category_id,
            ]);
        return [
        'status' => 'success',
        'source' => 'Source updated successfully!'
        ];
    }

    public function deleteSource(Request $request, $id)
    {
        $source = Source::find($id);
        $source->delete();

        $request->session()->flash('alert-success', 'Source successful deleted!');
        return redirect()->route("getSource");
    }
}
