<?php

namespace App\Http\Controllers;

use App\Image;
use App\Article;
use App\Source;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$categories = Category::all();
    	\View::share('categories', $categories);
    }

    public function index(Request $request) {
        $articles = Article::with('image')->orderBy('published_date', 'desc')->paginate(10);

        //latest articles for the slider and marquee
        $latestCategory = Category::where('name_en', 'Latest')->first();
        $slider = Article::whereCategoryId($latestCategory->id)->with('validImage')->orderBy('created_at', 'desc')->take(3)->get();
        //dd($slider[1]->image[0]);

        //other articles for the divers section
        $otherCategory = Category::where('name_en', 'Others')->first();
        $otherArticles = Article::whereCategoryId($otherCategory->id)->orderBy('published_date', 'desc')->paginate(3);

        //send all the sources to the front end
        $sources = Source::all();

    	return view('welcome', compact('articles', 'slider', 'sources', 'otherArticles'));
    }

    public function readArticle(Request $request, $id) {
        $article = Article::find($id);
        if(!$article) return \Redirect::back();

        //send all the sources to the front end
        $sources = Source::all();

        //other articles for the divers section
        $otherCategory = Category::where('name_en', 'Others')->first();
        $otherArticles = Article::whereCategoryId($otherCategory->id)->orderBy('published_date', 'desc')->paginate(3);

        return view('full_article', compact('article', 'sources', 'otherArticles'));
    }

    public function categoryArticles(Request $request, $id) {
        $category = Category::find($id);
        if(!$category) return \Redirect::back();

        $articles = Article::whereCategoryId($category->id)->orderBy('published_date', 'desc')->get();

        //send all the sources to the front end
        $sources = Source::all();

        //other articles for the divers section
        $otherCategory = Category::where('name_en', 'Others')->first();
        $otherArticles = Article::whereCategoryId($otherCategory->id)->orderBy('published_date', 'desc')->paginate(3);

        return view('category_articles', compact('category', 'articles', 'sources', 'otherArticles'));
    }

}
