<?php

namespace App\Http\Controllers;

use \Goutte\Client;
use App\Category;
use App\Source;
use App\Article;
use App\Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{

	public $links = array();
	public $count = 0;
	public $articles = array();
	public $image;
	public $source;
	public $article;
    public $image_link;
    public $page;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
    	$categories = Category::all();
    	\View::share('categories', $categories);
    }

    /*public function testscrapeArticle() {
    	$this->count = 0;
    	$client = new Client();
    	$crawler = $client->request('GET', 'http://www.crtv.cm/category/news/');

    	$crawler->filter('.entry-header .entry-title a')->each(function ($link) {
    		array_push($this->links, "http://crtv.cm/" . $link->attr("href"));
    	});

    	$this->links = array_unique($this->links);

    	foreach($this->links as $site) {
    		$newsCrawler = $client->request('GET', $site);
            $newsCrawler->filter('#content')->each(function($news) {
                $this->count = 0;
                $news->filter('article')->each(function($article) {
                    $article->filter('.article-content')->each(function($articleContent) {
                        $articleContent->filter('.entry-header h1')->each(function($title) {
                            if($this->count < 1) {
                                echo "Title: " . $title->text() . "<br><br>";
                                $this->count++;
                            }
                        });
                        $this->count = 0;
                        $articleContent->filter('.below-entry-meta .posted-on a time')->each(function($date) {
                            if($this->count < 1) {
                                echo "Date: " . $date->text() . "<br><br>";
                                $this->count++;
                            }
                        });
                        $this->count = 0;
                        $articleContent->filter('.featured-image')->each(function($image) {
                            var_dump($image);dd();
                            if($this->count < 1) {
                                echo "Image: " . "http://crtv.cm" . $image->attr('src') . "<br><br>";
                                $this->count++;
                            }
                        });
                        $this->count = 0;
                        $articleContent->filter('.entry-content p')->each(function($content) {
                            echo "Content: " . $content->text() . "<br><br>";
                            $this->count++;
                        });
                    });
                });
            });
        }
        return;
    }*/

    public function scrapeArticles() {
        $sources = Source::all();
        $client = new Client();
        $this->article = new Article();
        $this->image = new Image();

        foreach($sources as $source) {
            $this->source = $source;
            $crawler = $client->request('GET', $source->url);

            $crawler->filter('.entry-header .entry-title a')->each(function ($link) {
                array_push($this->links, $link->attr('href'));
            });

            $this->links = array_unique($this->links);

            foreach($this->links as $page) {
                $this->article = new Article();
                $this->image = new Image();
                $this->count = 0;

                $this->page = $page;
                $newsCrawler = $client->request('GET', $page);

                $newsCrawler->filter('#content')->each(function($news) {
                    $news->filter('article')->each(function($article) {

                        $article->filter('.article-content')->each(function($articleContent) {
                            $this->count = 0;
                            $articleContent->filter('.entry-header h1')->each(function($title) {
                                if($this->count < 1) {
                                    $this->article->title = $title->text();
                                    echo "Title: " . $title->text() . "<br><br>";
                                    $this->count++;
                                }
                            });

                            $this->count = 0;
                            $articleContent->filter('.below-entry-meta .posted-on a time')->each(function($date) {
                                if($this->count < 1) {
                                    $this->article->published_date = $date->text();
                                    echo "Date: " . $date->text() . "<br><br>";
                                    $this->count++;
                                }
                            });

                            $articleContent->filter('.entry-content p')->each(function($content) {
                                echo "Content: " . $content->text() . "<br><br>";
                                $this->article->content .= $content->text();
                            });
                        });

                        $this->count = 0;
                        $article->filter('.featured-image img')->each(function($image) {
                            if($this->count < 1) {
                                $this->image->url = $image->attr('src');
                                $this->image_link = $image->attr('src');
                                echo "Setting Image: " . $image->attr('src') . "<br><br>";
                                $this->count++;
                            }
                        });

                    });

                    $this->article->back_link = $this->page; 
                    $this->article->source_id = $this->source->id;
                    $this->article->category_id = $this->source->category_id;
                    $existingArticle = Article::whereTitle($this->article->title)->orWhere('back_link', $this->page)->first();
                    if(!$existingArticle) {
                        echo("New Article: "); var_dump($this->article->title); echo("<br><br>");
                        $this->article->save();
                    }else {
                        echo("Former Article: "); var_dump($existingArticle->id); echo("<br><br>");
                        $this->article->id = $existingArticle->id;
                        $this->article->update();
                    }

                    if($this->image_link) {
                        $this->image->name = $this->article->title;
                        $this->image->article_id = $this->article->id;
                        echo("Has the Image been set: "); var_dump($this->image_link); echo "<br>";
                        $existingImage = Image::whereUrl($this->image_link)->first();
                        if(!$existingImage) {
                            echo("New Image: "); var_dump($this->image_link); echo("<br><br>");
                            $this->image->url = $this->image_link;
                            $this->image->save();

                        }else {
                            echo("Former Image Id: "); var_dump($existingImage->id); echo("<br><br>");
                            $this->image->url = $this->image_link;
                            $this->image->id = $existingImage->id;
                            $this->image->update();
                        }
                    }
                    echo "Ending old article <br><br><br><br>";
                });
            } 
        }
        return;
    }

}
