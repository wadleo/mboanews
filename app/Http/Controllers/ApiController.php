<?php

namespace App\Http\Controllers;

use App\Category;
use App\Source;
use App\Article;
use App\Image;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    //
	public function getCategories(Request $request) {
		$categories = Category::all();
		$categories = $categories->map(function ($category) {
			$category->articles_count = $category->articlesCount();

			return collect($category->toArray())
			->only(['id', 'name_en', 'name_fr', 'description_en', 'description_fr', 'created_at', 'updated_at', 'articles_count'])
			->all();
		});

		return response()->json($categories, 200);
	}

	public function getSources(Request $request) {
		$sources = Source::all();

		return response()->json($sources, 200);
	}

	public function getCategoryArticles(Request $request, $id) {
		$category = Category::find($id);
		if(!$category) return request()->json(["error" => "Sorry category not found"], 404);

		$articles = Article::whereCategoryId($category->id)->orderBy('published_date', 'desc')->paginate(50);
		$articles = $articles->map(function ($articles) {
			return collect($articles->toArray())
			->only(['id', 'title', 'content', 'published_date', 'source_id', 'category_id', 'back_link', 'created_at', 'updated_at'])
			->all();
		});

		return response()->json($articles, 200);
	}

	public function getArticle(Request $request, $id=0) {
		$article = Article::find($id);
		if(!$article) return request()->json(["error" => "Sorry article not found"], 404);

		$article = collect($article->toArray())->only(['id', 'title', 'content', 'published_date', 'source_id', 'category_id', 'back_link', 'created_at', 'updated_at'])->all();

		return response()->json($article, 200);
	}

	public function getArticles(Request $request) {
		$article = Article::orderBy('published_date', 'desc')->paginate(50);

		return response()->json($article, 200);
	}

}
