<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'details', 'domain', 'category_id',
    ];

    //
    public function articles() {
        return $this->hasMany('App\Article');
    }

    public function category() {
        return $this->belongsTo('App\Category');
    }

}
