<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - MboaNews </title>

    <link rel="shortcut icon" type="image/ico" href="{{URL::to('images')}}/logo.gif">

    <link href="/css/bootstrap.css" rel='stylesheet' type='text/css'/>

    <!-- Custom Theme files -->
    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords"
    content="Get Local News Content in Cameroon. We gather news content from the famous news stations and companies to serve you the latest news"/>

    <!-- //for bootstrap working -->
    <!-- //for bootstrap working -->
    <!-- web-fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <div class="">
            <nav class="navbar navbar-default" role="navigation" style="border-color: black!important; border-radius: 0px;">
                <div class="">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#newsHeaders1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!--/.navbar-header-->

                    <div class="collapse navbar-collapse" id="newsHeaders1" style="padding: 0px!important;">
                        <!-- header-section-starts-here -->
                        <div class="header">
                            <div class="header-top">
                                <div class="wrap">
                                    <div class="top-menu">
                                        <ul class="nav navbar-nav">
                                            <li>
                                                <div class="row">
                                                    <div class="hidden-xs">
                                                        <a href="">
                                                            <img src="/images/logo.gif" alt=""width="50px" height="50px"/>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="active"><a href="/">Home</a></li>
                                            <li><a href="/about">About Us</a></li>
                                            <li><a href="/privcacy">Privacy Policy</a></li>
                                            <li><a href="/contact">Contact Us</a></li>
                                        </ul>
                                    </div>

                                    <!-- Right Side Of Navbar -->
                                    <ul class="nav navbar-nav navbar-right">
                                        <!-- Authentication Links -->
                                        @if (Auth::guest())
                                        <li class="@yield('login')" style="list-style: none;">
                                            <a href="{{ url('/login') }}">Login</a>
                                        </li>

                                        <li class="@yield('register')" style="list-style: none;">
                                            <a href="{{ url('/register') }}">Sign Up</a>
                                        </li>
                                        @else

                                        <li class="dropdown" style="list-style: none;">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu dropdown-content" role="menu">

                                                @if(Auth::user()->type==='admin')
                                                <li><a href="/dashboard">Admin Panel</a></li>
                                                @endif

                                                <li>
                                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                        Logout <i class="fa fa-sign-out"></i>
                                                    </a>

                                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>

                                        @endif
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </nav>
        </div>

        <div class="">
            <nav id="secondNav" class="navbar navbar-default" role="navigation">
                <div class="wrap">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#newsHeaders">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!--/.navbar-header-->

                    <div class="collapse navbar-collapse" id="newsHeaders">
                        <ul class="nav navbar-nav">
                            @foreach($categories as $category)
                            <li class="@yield($category->name_fr)">
                                <a href="/category/{{$category->id}}">{{$category->name_fr}}</a>
                            </li>
                            @endforeach
                            <div class="clearfix"></div>
                        </ul>
                        <div class="search">
                            <!-- start search-->
                            <div class="search-box">
                                <div id="sb-search" class="sb-search">
                                    <form>
                                        <input class="sb-search-input" placeholder="Enter your search term..."
                                        type="search" name="search" id="search" style="background-color: #2b303a; border-radius: 10px 10px 10px 10px;">
                                        <input class="sb-search-submit" type="submit" value="">
                                        <span class="sb-icon-search"> </span>
                                    </form>
                                </div>
                            </div>
                            <!-- search-scripts -->
                            <script src="/js/classie.js"></script>
                            <script src="/js/uisearch.js"></script>
                            <script>
                                new UISearch(document.getElementById('sb-search'));
                            </script>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </nav>
        </div>
    </div>

    <!-- header-section-ends-here -->

    @yield('content')

    <!-- footer-section-starts-here -->
    <div class="footer">
        <div class="footer-top">
            <div class="wrap">
                <div class="col-md-6 col-xs-8 col-sm-6 footer-grid">
                    <h4 class="footer-head">About Us</h4>
                    <p>We aim to provide the latest news articles in Cameroon.</p>
                    <p>We get articles from various news agencies our various communities to serve you with the latest content of news around the country.</p>
                </div>
                <div class="col-md-2 col-xs-4 col-sm-2 footer-grid">
                    <h4 class="footer-head">Categories</h4>
                    <ul class="cat">
                        <li><a href="">Latest</a></li>
                        <li><a href="">Politique</a></li>
                        <li><a href="">Economie</a></li>
                        <li><a href="">Arts & Culture</a></li>
                        <li><a href="">Sports</a></li>
                        <li><a href="">Divers</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-xs-12 col-sm-4 footer-grid">
                    <h4 class="footer-head">Contact Us</h4>
                    <span class="hq">Our headquaters</span>
                    <address>
                        <ul class="location">
                            <li><span class="glyphicon glyphicon-map-marker"></span></li>
                            <li>Mark Lynne Building, Malingo, Molyko, Buea</li>
                            <div class="clearfix"></div>
                        </ul>
                        <ul class="location">
                            <li><span class="glyphicon glyphicon-earphone"></span></li>
                            <li>+237 679 286 569</li>
                            <div class="clearfix"></div>
                        </ul>
                        <ul class="location">
                            <li><span class="glyphicon glyphicon-envelope"></span></li>
                            <li><a href="mailto:info@mboanews.com">info@mboanews.com</a></li>
                            <div class="clearfix"></div>
                        </ul>
                    </address>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="wrap">
                <div class="copyrights col-md-6">
                    <p> © 2017 MboaNews. All Rights Reserved | Design by <a href="http://wadleo.com/">MboaNews Team</a></p>
                </div>
                <div class="footer-social-icons col-md-6">
                    <ul>
                        <li><a class="facebook" href="#"></a></li>
                        <li><a class="twitter" href="#"></a></li>
                        <li><a class="flickr" href="#"></a></li>
                        <li><a class="googleplus" href="#"></a></li>
                        <li><a class="dribbble" href="#"></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- footer-section-ends-here -->

    <!-- Scripts -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <!-- for bootstrap working -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>

    <script type="application/x-javascript"> addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
        window.scrollTo(0, 1);
    } </script>

    <script type="text/javascript" src="/js/responsiveslides.min.js"></script>
    <script type="text/javascript" src="/js/move-top.js"></script>
    <script type="text/javascript" src="/js/easing.js"></script>
    <script type="text/javascript" src="/js/jquery.marquee.min.js"></script>

    <script type="text/javascript">
        $(function () {

            $(window).scroll(function (event) {
                var scroll = $(window).scrollTop();
            // Do something
            if (scroll > 70) {
                $('#secondNav').addClass('navbar-fixed-top');
            }else {
                $('#secondNav').removeClass('navbar-fixed-top');
            }
        });

            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({scrollTop: $(this.hash).offset().top}, 900);
            });

            var defaults = {
            wrapID: 'toTop', // fading element id
            wrapHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 5000,
            easingType: 'linear'
        };

        $().UItoTop({easingType: 'easeOutQuart'});

    });
</script>

@yield('scripts')

<!-- <script src="{{ asset('js/app.js') }}"></script> -->
<a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!---->
</body>
</html>
