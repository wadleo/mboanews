@extends('layouts.app')

@section('title', 'Welcome')

@section('Nouvelles', 'active')

@section('content')

<div class="wrap">
    <div class="move-text">
        <div class="breaking_news">
            <h2>Breaking News</h2>
        </div>
        <div class="marquee">
            @foreach($slider as $slide)
            <div class="" style="display: inline;"><a class="breaking" href="/article/{{$slide->id}}"> >>{{$slide->title}}...</a></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;              
            @endforeach
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<!-- content-section-starts-here -->
<div class="main-body">
    <div class="wrap">
        <div class="col-md-8 content-left">
            <div class="slider">
                <div class="callbacks_wrap">
                    <ul class="rslides" id="slider">
                        @foreach($slider as $slide)
                        @if($slide->hasImage())
                        <li>
                            <a href="/article/{{$slide->id}}">
                                <img src="{{$slide->image->url}}" alt="{{$slide->image->name}}">
                            </a>
                            <div class="caption">
                                <a href="/article/{{$slide->id}}">{{$slide->title}}</a>
                            </div>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="articles">
                <header>
                    <h3 class="title-head">Latest headlines</h3>
                </header>
                @foreach($articles as $article)
                <div class="article">
                    @if($article->hasImage())
                    <div class="article-left">
                        <a href="/article/{{$article->id}}">
                            <img src="{{$article->image->url}}" alt="{{$article->image->name}}">
                        </a>
                    </div>
                    @endif
                    <div @if($article->hasImage()) class="article-right" @endif>
                        <div class="article-title">
                            <p>On {{$article->published_date}} <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>104 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-thumbs-up"></span>52</a></p>
                            <a class="title" href="/article/{{$article->id}}"> {{$article->title}}</a>
                        </div>
                        <div class="article-text">
                            <p>{{str_limit($article->content, 120)}}</p>
                            <a href="/article/{{$article->id}}"><img src="/images/more.png" alt="" /></a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @endforeach
            </div>
        </div>

        <div class="col-md-4 side-bar">
            <div class="first_half">
                <div class="newsletter">
                    <h1 class="side-title-head">Newsletter</h1>
                    <p class="sign">Sign up to receive our free newsletters!</p>
                    <form>
                        <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}">
                        <input type="submit" value="submit">
                    </form>
                </div>

                <div class="list_vertical">
                    <section class="accordation_menu">
                        <!-- Others section on the side bar -->
                        <div>
                            <input id="label-1" name="lida" type="radio" checked/>
                            <label for="label-1" id="item1">
                                <i class="ferme"> </i>Divers<i class="icon-plus-sign i-right1"></i>
                                <i class="icon-minus-sign i-right2"></i>
                            </label>
                            <div class="content" id="a1">
                                <div class="scrollbar" id="style-2">
                                    <div class="force-overflow">
                                        <div class="popular-post-grids">
                                            @foreach($otherArticles as $article)
                                            <div class="popular-post-grid">
                                                @if($article->hasImage())
                                                <div class="post-img">
                                                    <a href="/article/{{$article->id}}">
                                                        <img src="{{$article->image->url}}" alt="{{$article->image->name}}" />
                                                    </a>
                                                </div>
                                                @endif
                                                <div class="post-text">
                                                    <a class="pp-title" href="/article/{{$article->id}}"> {{$article->title}}</a>
                                                    <p>On {{$article->published_date}} <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>3 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Comments Section on the side bar -->
                        <div>
                            <input id="label-3" name="lida" type="radio"/>
                            <label for="label-3" id="item3">
                                <i class="icon-trophy" id="i3"></i>Comments<i class="icon-plus-sign i-right1"></i>
                                <i class="icon-minus-sign i-right2"></i>
                            </label>
                            <div class="content" id="a3">
                                <div class="scrollbar" id="style-2">
                                    <div class="force-overflow">
                                        <div class="response">
                                            <div class="media response-info">
                                                <div class="media-left response-text-left">
                                                    <!-- <a href="#">
                                                        <img class="media-object" src="images/icon1.png" alt="" />
                                                    </a> -->
                                                    <h5><a href="#">Username</a></h5>
                                                </div>
                                                <div class="media-body response-text-right">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                    <ul>
                                                        <li>MARCH 21, 2015</li>
                                                        <li><a href="">Reply</a></li>
                                                    </ul>
                                                </div>
                                                <div class="clearfix"> </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>

            <div class="secound_half">
                <div class="tags">
                    <header>
                        <h3 class="title-head">Categories</h3>
                    </header>
                    <p>
                        @foreach($categories as $category)
                        <a class="tag1" href="/category/{{$category->id}}">{{$category->name_fr}}</a>
                        @endforeach
                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- content-section-ends-here -->
@endsection

@section('scripts')

<script type="text/javascript">

    $('.marquee').marquee({pauseOnHover: true, speed: 10000});

    $("#slider").responsiveSlides({
        auto: true,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        pager: true,
    });

</script>

@endsection