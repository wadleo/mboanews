@extends('layouts.app')

@section('title', $article->title )

@section($article->category->name_fr, 'active')

@section('content')

<!-- content-section-starts-here -->
<div class="main-body">
    <div class="wrap">
        <div class="col-md-8 content-left">
            
            <div class="articles">
                <header>
                    <h3 class="title-head">{{$article->title}}</h3>
                </header>
                <div class="article">
                    @if($article->hasImage())
                    <div class="article-left">
                        <a href=""><img src="{{$article->image->url}}"></a>
                    </div>
                    @endif
                    <div>
                        <div class="article-title">
                            <p>On {{$article->published_date}} <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>0 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>104 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-thumbs-up"></span>52</a></p>
                        </div>
                        <div class="article-text">
                            <p style="font-size: 15px;">{{$article->content}}</p>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 side-bar">
            <div class="first_half">
                <div class="newsletter">
                    <h1 class="side-title-head">Newsletter</h1>
                    <p class="sign">Sign up to receive our free newsletters!</p>
                    <form>
                        <input type="text" class="text" value="Email Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}">
                        <input type="submit" value="submit">
                    </form>
                </div>

                <div class="list_vertical">
                    <section class="accordation_menu">
                        <!-- Others section on the side bar -->
                        <div>
                            <input id="label-1" name="lida" type="radio" checked/>
                            <label for="label-1" id="item1"><i class="ferme"> </i>Divers<i class="icon-plus-sign i-right1"></i><i class="icon-minus-sign i-right2"></i></label>
                            <div class="content" id="a1">
                                <div class="scrollbar" id="style-2">
                                    <div class="force-overflow">
                                        <div class="popular-post-grids">
                                            @foreach($otherArticles as $otherArticle)
                                            <div class="popular-post-grid">
                                                @if($otherArticle->hasImage())
                                                <div class="post-img">
                                                    <a href="/article/{{$otherArticle->id}}"><img src="{{$otherArticle->image->url}}" alt="" /></a>
                                                </div>
                                                @endif
                                                <div class="post-text">
                                                    <a class="pp-title" href="/article/{{$otherArticle->id}}"> {{$otherArticle->title}}</a>
                                                    <p>On {{$otherArticle->published_date}} <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>3 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Recent Articles on the side bar -->
                            <!-- <div>
                                <input id="label-2" name="lida" type="radio"/>
                                <label for="label-2" id="item2"><i class="icon-leaf" id="i2"></i>Recent Articles<i class="icon-plus-sign i-right1"></i><i class="icon-minus-sign i-right2"></i></label>
                                <div class="content" id="a2">
                                    <div class="scrollbar" id="style-2">
                                        <div class="force-overflow">
                                            <div class="popular-post-grids">
                                                <div class="popular-post-grid">
                                                    <div class="post-img">
                                                        <a href="single.html"><img src="images/tec2.jpg" alt="" /></a>
                                                    </div>
                                                    <div class="post-text">
                                                        <a class="pp-title" href="single.html"> The section of the mass media industry</a>
                                                        <p>On Feb 25 <a class="span_link" href="#"><span class="glyphicon glyphicon-comment"></span>3 </a><a class="span_link" href="#"><span class="glyphicon glyphicon-eye-open"></span>56 </a></p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <!-- Comments Section on the side bar -->
                            <div>
                                <input id="label-3" name="lida" type="radio"/>
                                <label for="label-3" id="item3"><i class="icon-trophy" id="i3"></i>Comments<i class="icon-plus-sign i-right1"></i><i class="icon-minus-sign i-right2"></i></label>
                                <div class="content" id="a3">
                                    <div class="scrollbar" id="style-2">
                                        <div class="force-overflow">
                                            <div class="response">
                                                <div class="media response-info">
                                                    <div class="media-left response-text-left">
                                                        <!-- <a href="#">
                                                            <img class="media-object" src="images/icon1.png" alt="" />
                                                        </a> -->
                                                        <h5><a href="#">Username</a></h5>
                                                    </div>
                                                    <div class="media-body response-text-right">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                            <ul>
                                                                <li>MARCH 21, 2015</li>
                                                                <li><a href="">Reply</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                    </div>

                    <div class="secound_half">
                        <div class="tags">
                            <header>
                                <h3 class="title-head">Categories</h3>
                            </header>
                            <p>
                                @foreach($categories as $category)
                                <a class="tag1" href="/category/{{$category->id}}">{{$category->name_fr}}</a>
                                @endforeach
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- content-section-ends-here -->


        @endsection