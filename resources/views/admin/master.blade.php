<!DOCTYPE HTML>
<html>
<head>

    <title>MboaNews | Admin Panel</title>
    <link rel="shortcut icon" type="image/ico" href="{{URL::to('images')}}/logo.gif">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="MboaNews Admin Panel" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="/css/admin.css" rel='stylesheet' type='text/css' />
    <!-- Graph CSS -->
    <link href="/css/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'/>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <!-- lined-icons -->
    <link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
</head>
<body>
    <div class="left-content">
        <div class="inner-content">
            <!-- header-starts -->
            <div class="header-section">
                <!-- top_bg -->
                <div class="top_bg">

                    <div class="header_top">
                        <div class="top_right">
                            <ul>
                                <li><a href="">Help</a></li>|
                                <li><a href="">Contact</a></li>|
                                <li><a href="">About</a></li>
                            </ul>
                        </div>
                        <div class="top_left">
                            <h2><span></span> Call us : 679286569</h2>
                        </div>
                        <div class="clearfix"> </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <!-- /top_bg -->
            </div>

            <!-- //header-ends -->

            <!--content-->
            <div class="content">

                @yield('content')

                <div class="clearfix"></div>

                <div class="fo-top-di">
                    <div class="footer">
                        <div class="col-md-6 cust">
                            <h4>CUSTOMER CARE</h4>
                            <li><a href="">Help Center</a></li>
                            <li><a href="">FAQ</a></li>
                        </div>
                        <div class="col-md-6 our-st">
                            <div class="our-left">
                                <h4>CONTACT US</h4>
                            </div>

                            <li><i class="add"> </i>The MboaNews Team</li>
                            <li><i class="phone"> </i>679286569</li>
                            <li><a href="mailto:info@mboanews.com"><i class="mail"> </i>info@mboanews.com </a></li>
                        </div>
                        <div class="clearfix"> </div>
                        <p>© 2016 MboaNews. All Rights Reserved | Design by <a href="">MboaNews Team</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="sidebar-menu">
            <header class="logo1">
                <a href="">
                    <!-- <img src="/images/logo.gif" alt="" width="35px" height="35px" /> -->
                    <h4 style="color: #fff; display: inline;">Admin MboaNews</h4>
                </a>
                <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a>
            </header>
            <div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
            <div class="menu">
                <ul id="menu" >
                    <li><a href="/dashboard">
                        <i class="fa fa-tachometer"></i> <span>Home</span></a>
                    </li>
                    <li id="menu-academico" ><a href="{{ route('getSource') }}">
                        <i class="fa fa-pencil"></i> <span> Sources</span></a>
                    </li>
                    <li id="menu-academico" >
                        <a href="#">
                            <i class="fa fa-table"></i> 
                            <span> Articles</span> 
                            <span class="fa fa-angle-right" style="float: right"></span>
                        </a>
                        <ul id="menu-academico-sub" >
                            <li ><a href="">Nouvelles</a></li>
                            <li ><a href="">Politique</a></li>
                            <li ><a href="">Economie</a></li>
                            <li ><a href="">Culture</a></li>
                            <li ><a href="">Sport</a></li>
                            <li ><a href="">Divers</a></li>
                        </ul>
                    </li>
                    <li id="menu-academico" ><a href="">
                        <i class="fa fa-pencil"></i> <span> Categories</span></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!--js -->
    <script src="/js/amcharts.js"></script>
    <script src="/js/serial.js"></script>
    <script src="/js/light.js"></script>
    <!-- //lined-icons -->
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/jquery.nicescroll.js"></script>
    <script src="/js/scripts.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>
    <!-- /Bootstrap Core JavaScript -->
    <!-- real-time -->
    <script language="javascript" type="text/javascript" src="/js/jquery.flot.js"></script>

    <!-- /real-time -->
    <script src="js/jquery.fn.gantt.js"></script>

    <script src="/js/menu_jquery.js"></script>

    <script>
        $(function() {

            var toggle = true;

            $(".sidebar-icon").click(function() {
                if (toggle)
                {
                    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                    $("#menu span").css({"position":"absolute"});
                }
                else
                {
                    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                    setTimeout(function() {
                        $("#menu span").css({"position":"relative"});
                    }, 400);
                }

                toggle = !toggle;
            });

            // We use an inline data source in the example, usually data would
            // be fetched from a server

            var data = [],
            totalPoints = 300;

            function getRandomData() {

                if (data.length > 0)
                    data = data.slice(1);

                // Do a random walk

                while (data.length < totalPoints) {

                    var prev = data.length > 0 ? data[data.length - 1] : 50,
                    y = prev + Math.random() * 10 - 5;

                    if (y < 0) {
                        y = 0;
                    } else if (y > 100) {
                        y = 100;
                    }

                    data.push(y);
                }

                // Zip the generated y values with the x values

                var res = [];
                for (var i = 0; i < data.length; ++i) {
                    res.push([i, data[i]])
                }

                return res;
            }

            // Set up the control widget

            var updateInterval = 30;

            $("#updateInterval").val(updateInterval).change(function () {
                var v = $(this).val();
                if (v && !isNaN(+v)) {
                    updateInterval = +v;
                    if (updateInterval < 1) {
                        updateInterval = 1;
                    } else if (updateInterval > 2000) {
                        updateInterval = 2000;
                    }
                    $(this).val("" + updateInterval);
                }
            });

        });

    </script>

    @yield('script')

</body>
</html>