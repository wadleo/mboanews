@extends('admin.master')

@section('content')

<div class="monthly-grid">
    <div class="panel panel-widget">
        <div class="panel-title">
            Update/edit Source!
            <a href="{{ route('getSource') }}" class="btn btn-danger pull-right" style="color: #fff !important;"><i class="fa fa-reply"></i> Back</a>
        </div>
        <div class="panel-body">
            <!-- status -->
            <div class="contain">
                <div class="gantt"></div>
            </div>
            <!-- status -->
        </div>
    </div>
</div>
<div class="monthly-grid">
    <div class="panel panel-widget">
        <div class="panel-title">
            <div class="alert formalert" style="display: none;"></div>

        </div>
        <div class="panel-body">
            <!-- status -->
            <div class="contain">
                <form action="/dashboard/update/source/{{$source->id}}" id="updateForm" class="form-horizontal" role="form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="{{ ($source->name) }}" required="required" id="name" style="color: #000 !important;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL</label>
                        <div class="col-sm-10">
                            <input type="url" required="required" name="url" value="{{ ($source->url) }}" class="form-control" id="url" style="color: #000 !important;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="domain" class="col-sm-2 control-label">Domain</label>
                        <div class="col-sm-10">
                            <input type="url" required="required" name="domain" value="{{ ($source->domain) }}" class="form-control" id="domain" style="color: #000 !important;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="details" class="col-sm-2 control-label">Details</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="details" name="details" style="color: #000 !important;">{{$source->details}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="category_id" style="color: #000 !important;" id="category">
                                @foreach($categories as $category)
                                <option value="{{$category->id}}" @if($category->id == $source->category_id) selected="selected" @endif>{{$category->name_fr}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 pull-right">
                            <button type="submit" class="btn btn-danger updateButton">Update</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- status -->
        </div>
    </div>
</div>
@endsection

@section('script');
<script type="text/javascript"> 

    $('#updateForm').submit(function (evt) {
        evt.preventDefault();
        $.ajax('{{ route('updateSource', ['id' => $source->id])}}', {
            method: 'POST',
            data: {
                _token: '{{csrf_token()}}',
                name: $('#name').val(),
                url: $('#url').val(),
                details: $('#details').val(),
                domain: $('#domain').val(),
                details: $('#details').val(),
                category_id: $('#category').val()
            },
            cache: false,
            success: function (data) {
                if (data.status === 'success') {
                    $('.formalert').removeClass('alert-danger').addClass('alert-success').html(data.source).show(300);
                }
            },
            error: function (err) {
                if (err.status == 422) {
                    var errors = JSON.parse(err.responseText);
                    var html = '<ol style="margin-bottom:0;">';
                    for (var index in errors) {
                        if (errors[index].length > 1) {
                            for (var i in errors[index])
                                html += '<li>' + errors[index][i] + '</li>';
                        } else
                        html += '<li>' + errors[index] + '</li>';
                    }
                    html = html + '</ol>';
                    $('.formalert').removeClass('alert-success').addClass('alert-danger').html(html).show(300);

                }
            }
        });
    });
</script>

@endsection