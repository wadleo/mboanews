@extends('admin.master')

@section('content')

<div class="monthly-grid">
    <div class="panel panel-widget">
        <div class="panel-title">
            Publish new Sources!<a href="{{ route('admin') }}" class="btn btn-danger pull-right" style="color: #fff !important;"><i class="fa fa-reply"></i> Back</a>
        </div>
        <div class="panel-body">
            <!-- status -->
            <div class="contain">
                <div class="gantt"></div>
            </div>
            <!-- status -->
        </div>
    </div>
</div>
<div class="monthly-grid">
    <div class="panel panel-widget">
        <div class="panel-title">
            <div class="alert formalert" style="display: none;"></div>

            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-'. $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-'. $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif
                @endforeach
            </div> <!-- end .flash-message -->

        </div>
        <div class="panel-body">
            <!-- status -->
            <div class="contain">
                <form action="{{ route('createSource') }}" id="sourceForm" class="form-horizontal" role="form" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" required="required" id="name" placeholder="Type in source name here..." style="color: #000 !important;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="url" class="col-sm-2 control-label">URL</label>
                        <div class="col-sm-10">
                            <input type="url" required="required" name="url" class="form-control" id="url" placeholder="Enter url here..." style="color: #000 !important;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="domain" class="col-sm-2 control-label">Domain</label>
                        <div class="col-sm-10">
                            <input type="url" required="required" name="domain" class="form-control" id="domain" placeholder="Enter source domain here" style="color: #000 !important;">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="details" class="col-sm-2 control-label">Details</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="details" name="details" placeholder="Enter source details here" style="color: #000 !important;"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="category" class="col-sm-2 control-label">Category</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="category_id" style="color: #000 !important;" id="category">
                                <option value="">Select a category</option>
                                @foreach($categories as $category)
                                <option value="{{$category->id}}" >{{$category->name_fr}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 pull-right">
                            <button type="submit" class="btn btn-danger">Publish</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- status -->
        </div>
    </div>
</div>


<div class="monthly-grid">
    <div class="panel panel-widget">
        <div class="panel-title">
            Recently published Sources!
        </div>
        <div class="panel-body">
            <!-- status -->
            <div class="contain">
                <div class="gantt">

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Domain</th>
                                <th>URL</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        @foreach($sources as $source)
                        <tbody>
                            <tr>
                                <td>{{ $source->name }}</td>
                                <td>{{ $source->domain }}</td>
                                <td>{{ $source->url }}</td>
                                <td>{{ $source->category->name_fr }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <a href="/dashboard/getedit/source/{{$source->id}}" class="btn btn-warning ">Edit</a>
                                        </div>
                                        <div class="col-lg-6">
                                            <a href="/dashboard/delete/source/{{$source->id}}" class="btn btn-danger"> Delete</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>

                        @endforeach

                    </table>

                </div>
            </div>
            <!-- status -->
        </div>
    </div>
</div>


<!-- Modal to reject and delete a source -->
{{--<div id="deleteModal{{$source->id}}" class="modal fade" role="dialog">--}}
{{--<div class="modal-dialog">--}}

{{--<div class="modal-content">--}}
{{--<div class="modal-header" STYLE="background-color: #52b6ec;">--}}
{{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--<h4 class="modal-title text-center" style="color:red;"><i class="fa fa-warning"--}}
{{--style="color: red;"></i>--}}
{{--Delete Source from DB!</h4>--}}
{{--</div>--}}
{{--<div class="modal-body">--}}
{{--<p class="text-center">Are you sure you wanna delete this source with name {{ $source->name }}?</p>--}}
{{--<p class="text-center"><a href="/dashboard/delete/source/{{$source->id}}"--}}
{{--class="btn btn-danger"><i class="fa fa-trash-o"></i>--}}
{{--Delete</a></p>--}}
{{--</div>--}}
{{--<div class="modal-footer">--}}
{{--<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}

<script type="text/javascript">

    $('#sourceForm').submit(function (e) {
        e.preventDefault();
        $('.formalert').hide(100).html('');
        alert($('#category').val());
        $.ajax('{{route("createSource")}}', {
            data: {
                _token: '{{csrf_token()}}',
                name: $('#name').val(),
                url: $('#url').val(),
                details: $('#details').val(),
                domain: $('#domain').val(),
                category_id: $('#category').val()
            },
            method: 'POST',
            cache: false,
            success: function (data) {
                if (data.status === 'success') {
                    $('.formalert').removeClass('alert-danger').addClass('alert-success').html(data.source).show(300);
                    $('#name').val('');
                    $('#details').val('');
                    $('#domain').val('');
                    $('#url').val('');
                    $('#category').val('');
                }
            },
            error: function (err) {
                if (err.status == 422) {
                    var errors = JSON.parse(err.responseText);
                    var html = '<ol style="margin-bottom:0;">';
                    for (var index in errors) {
                        if (errors[index].length > 1) {
                            for (var i in errors[index])
                                html += '<li>' + errors[index][i] + '</li>';
                        } else
                        html += '<li>' + errors[index] + '</li>';
                    }
                    html = html + '</ol>';
                    $('.formalert').removeClass('alert-success').addClass('alert-danger').html(html).show(300);

                }
            }
        });
    });

</script>

@endsection